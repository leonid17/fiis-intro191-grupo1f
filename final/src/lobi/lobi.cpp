/*
 * prob01.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: JC
 */
#include <iostream>
#include "../combos/combos.hpp"
#include "../lobi/lobi.hpp"
#include "../deli/deli.hpp"
#include "../cart/cart.hpp"
#include "../ord/ord.hpp"
using namespace std;

void lobi(){
	int a;
	cout << "Bienvenido a la polleria El Huesito"<< endl;
	cout<<"1.- Combos disponibles"<<endl;
	cout<<"2.- Orden delivery"<< endl;
	cout<<"3.- Orden en el restaurante"<< endl; /*para ir al restaurante a comer*/
	cout<<"4.- Salir"<<endl;
	cout<<"Ingrese opcion: ";
	cin>>a;
	if(a == 1){
		combos();

	}else if(a == 2){
		deli();

	}else if(a == 3){
		cart();

	}else if(a == 4){
		cout<<"Adios";

	}else if(a < 0 || a>4){
		cout<<"Ingrese opcion valida"<< endl;
		lobi();

	}
}




